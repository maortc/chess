#pragma once
#include "piece.h"

#define FIRST_MOVE_SIZE 2
#define RESET_MOVE_SIZE 1

class pawn : public piece
{
public:
	pawn();
	pawn(int x, int y, bool color, int type);
	~pawn();
	virtual bool isMoveValid(int x, int y, board& cBboard);
private:
	bool isFirstTurn;
};

