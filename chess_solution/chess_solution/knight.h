#pragma once
#include "piece.h"  
class knight : public piece
{
public:
	knight();
	knight(int x, int y, bool color, int type);
	~knight();
	virtual bool isMoveValid(int x, int y, board& cBboard);
};

