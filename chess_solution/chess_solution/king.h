#pragma once
#include "piece.h"
class king : public piece
{
public:
	king();
	king(int x, int y, bool color, int type);
	~king();
	virtual bool isMoveValid(int x, int y, board& cBboard);
};

