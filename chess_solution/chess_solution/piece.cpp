#include "piece.h"

piece::piece()
{
	this->_x = 0;
	this->_y = 0;
	this->_color = "";
	this->_type = 0;
}

piece::piece(int x, int y, bool color, int type)
{
	this->_x = x;
	this->_y = y;
	this->_color = color;
	this->_type = type;
}

piece::~piece()
{
}
/*
The function ruturns x value
input: none
output: x value
*/
int piece::getX()
{
	return this->_x;
}
/*
The function set x value
input: x value
output: none
*/
void piece::setX(int x)
{
	this->_x = x;
}
/*
The function ruturns y value
input: none
output: y value
*/
int piece::getY()
{
	return this->_y;
}
/*
The function set y value
input: y value
output: none
*/
void piece::setY(int y)
{
	this->_y = y;
}
/*
The function ruturns color value
input: none
output: color value
*/
bool piece::getColor()
{
	return this->_color;
}
/*
The function set color value
input: color value
output: none
*/
void piece::setColor(bool color)
{
	this->_color = color;
}
/*
The function ruturns type value
input: none
output: type value
*/
int piece::getType()
{
	return this->_type;
}
/*
The function set type value
input: type value
output: none
*/void piece::setType(int type)
{
	this->_type = type;
}
