#include "board.h"
#include <string>
#include <iostream>

board::board()
{
	int x = 0;
	int y = 0;

	// insert nulptr in all board cells
	for (y = 0; y < ROW_LEN; y++)
	{
		for (x = 0; x < COL_LEN; x++)
		{
			this->_pBoard[x][y] = nullptr;
		}
	}
	this->_turn = WHITE;
}

board::~board()
{
	int x = 0;
	int y = 0;

	// delete all board cells
	for (y = 0; y <ROW_LEN; y++)
	{
		for (x = 0; x < COL_LEN; x++)
		{
			delete this->_pBoard[x][y];
		}
	}
}
/*
the function checks if a check accoured
input : kings color
output: if check accoured
*/
bool board::isCheck(bool kingColor)
{
	int x = 0;
	int y = 0;
	int kingX = 0;
	int kingY = 0;

	// gets king loction and save king location int vars kingX and kingY
	for (y = 0; y < ROW_LEN; y++)
	{
		for (x = 0; x < COL_LEN; x++)
		{
			if (this->getCell(x, y) != nullptr)
			{
				if (this->getCell(x, y)->getColor() == kingColor && this->getCell(x, y)->getType() == KING)
				{
					kingX = x;
					kingY = y;
					break;
				}
			}
		}
	}
	// this part of the code go throw all the pieces with the opeset color of the king and send to there isMoveValid function
	// king's x and y and if one of then returns true that means that there is a check
	for (y = 0; y < ROW_LEN; y++)
	{
		for (x = 0; x < COL_LEN; x++)
		{
			if (this->getCell(x, y) != nullptr) // make sure that cell isn't empty to avoid probloms when using getColor
			{
				if (this->getCell(x, y)->getColor() != kingColor) // make sure that the piece is opeset color of the king
				{
					if (this->getCell(x, y)->isMoveValid(kingX, kingY, *this) == true) // checks if piece can reace king
					{
						return true;
					}
				}
			}
		}
	}
	return false;
}

/*
The function returns a value according to the correctness of the move, board will by updetaed
input: current cell and new cell
output: value according to the correctness of the move
*/
int board::getCode(int currX, int currY, int newX, int newY)
{
	if (this->isPointInRange(newX, newY) == false) // checks if new point in range
	{
		return INVALID_INDEX;
	}
	if (this->_pBoard[currX][currY] == NULL || this->_pBoard[currX][currY]->getColor() != this->getTurn()) // checking if curr point isn't empty
	{
		return EMPTY;
	}
	if (this->_pBoard[newX][newY] != nullptr && this->_pBoard[currX][currX] != nullptr)// makes sure that both cells aren't empty to avoid probloms with getColor
	{
		if (this->_pBoard[currX][currY]->getColor() == this->_pBoard[newX][newY]->getColor()) // checking if des point and src point are diffrent color
		{
			return SAME_COLOR;
		}
	}
	if (this->_pBoard[currX][currY]->isMoveValid(newX, newY, *this) == false) // checks if the move is valid 
	{
		return INVALID_MOVE;
	}
	if (this->isPointSameLoction(currX, currY, newX, newY) == true) // checks if curr cell and new cell are the same
	{
		return SAME_INDEX;
	}
	// make move on board for check test, decaided by isCheck result if will be changed back of not
	if (this->_pBoard[newX][newY] != nullptr)
	{
		delete this->_pBoard[newX][newY];
	}
	this->_pBoard[newX][newY] = this->_pBoard[currX][currY];
	this->_pBoard[newX][newY]->setX(newX);
	this->_pBoard[newX][newY]->setY(newY);
	this->_pBoard[currX][currY] = nullptr;
	if (isCheck(!this->getTurn()))
	{
		this->_turn = (this->_turn == true) ? false: true; // switch turn
		return VALID_CHECK;
	}
	if (isCheck(this->getTurn()))
	{
		// becuase move is invalid board will be changed back
		this->_pBoard[currX][currY] = this->_pBoard[newX][newY];
		this->_pBoard[currX][currY]->setX(currX);
		this->_pBoard[currX][currY]->setY(currY);
		this->_pBoard[newX][newY] = nullptr;
		return INVALID_CHECK;
	}
	this->_turn = (this->_turn == true) ? false : true; // switch turn
	return VALID_MOVE;
}

/*
checks if point in range
input: point
output: if point in range
*/
bool board::isPointInRange(int x, int y)
{
	return y < COL_LEN && x < ROW_LEN && y >= 0 && x >= 0;
}
/*
checks if points in same location
input: points
output: is the points the same
*/
bool board::isPointSameLoction(int currX, int currY, int newX, int newY)
{
	return currX == newX && currY == newY;
}
/*
the function returns a cell by location
input: cell loction
output: the cell
*/
piece* board::getCell(int x, int y)
{
	return this->_pBoard[x][y];
}
/*
the function set a cell by loction
input: cell loction and value
output: none
*/
void board::setCell(int x, int y, piece* newPiece)
{
	this->_pBoard[x][y] = newPiece;
}
 /*
 the function set the turn by value
 input: new turn value
 output: none
 */
void board::setTurn(bool newTurn)
{
	this->_turn = newTurn;
}
/*
the function returns the turn value
input: none
output: turn value
*/
bool board::getTurn()
{
	return this->_turn;
}
