#include "king.h"
#include "board.h"


king::king()
{
}
king::king(int x, int y, bool color, int type) :
	piece(x, y, color, type)
{
}
king::~king()
{
}

/*
the function checks if attempted move is valid
input: x and y of attempted and board
output: is move valid
*/
bool king::isMoveValid(int x, int y, board& cBboard)
{
	if (cBboard.getCell(x, y) != nullptr) // if there is a piece in destantion 
	{
		if (cBboard.getCell(x, y)->getColor() == cBboard.getCell(_x, _y)->getColor()) // check if pieces are the same color
		{
			return false;
		}
	}
	return ((_x - x) * (_x - x) + (_y - y) * (_y - y) <= 2);// formula taken from https://www.geeksforgeeks.org/total-position-where-king-can-reach-on-a-chessboard-in-exactly-m-moves/
}
