#include "queen.h"
#include <iostream>

queen::queen()
{
}
queen::queen(int x, int y, bool color, int type) :
	piece(x, y, color, type)
{
}
queen::~queen()
{
}

/*
the function checks if attempted move is valid
input: x and y of attempted and board
output: is move valid
*/
bool queen::isMoveValid(int x, int y, board& cBboard)
{
	int i = 0;
	int dx = 0;
	int dy = 0;
	int indexX = _x;
	int indexY = _y;

	if (_x == x)// checks if the move is horizontal move
	{
		int dx = (_y < y) ? 1 : -1;
		for (i = _y + dx; i != y; i += dx)
		{
			if (cBboard.getCell(_x, i) != nullptr)
			{
				return false;
			}
		}
	}
	else if (_y == y)// check if the move is vrtical move
	{
		int dy = (_x < x) ? 1 : -1;
		for (i = _x + dy; i != x; i += dy)
		{
			if (cBboard.getCell(i, _y) != nullptr)
			{
				return false;
			}
		}
	}
	else // diagonal line
	{
		dx = (_x < x) ? 1 : -1;
		dy = (_y < y) ? 1 : -1;
		while (indexX != x && indexY != y)
		{
			indexX += dx;
			indexY += dy;
			if (indexX == x && indexY == y) // checks if reaced target cell
			{
				return true;
			}
			if (cBboard.getCell(indexX, indexY) != nullptr)
			{
				return false;
			}
		}
		return false;
	}
	return true;
}
