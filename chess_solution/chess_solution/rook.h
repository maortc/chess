#pragma once
#include "piece.h"
class rook : public piece
{
public:
	rook();
	rook(int x, int y, bool color, int type);
	~rook();
	virtual bool isMoveValid(int x, int y, board& cBboard);
};

