#include "knight.h"

knight::knight()
{
}
knight::knight(int x, int y, bool color, int type) :
	piece(x, y, color, type)
{
}
knight::~knight()
{
}

/*
the function checks if attempted move is valid
input: x and y of attempted and board
output: is move valid
*/
bool knight::isMoveValid(int x, int y, board& cBboard)
{
	return((std::abs(_x - x) == 2 && std::abs(_y - y) == 1) || (std::abs(_x - x) == 1 && std::abs(_y - y) == 2));
}
