#pragma once
#include "piece.h"
class queen : public piece
{
public:
	queen();
	queen(int x, int y, bool color, int type);
	~queen();
	virtual bool isMoveValid(int x, int y, board& cBboard);
};

