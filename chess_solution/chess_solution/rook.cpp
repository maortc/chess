#include "rook.h"

rook::rook()
{
}
rook::rook(int x, int y, bool color, int type) :
	piece(x, y, color, type)
{
}
rook::~rook()
{
}

/*
the function checks if attempted move is valid
input: x and y of attempted and board
output: is move valid
*/
bool rook::isMoveValid(int x, int y, board& cBboard)
{
	int i = 0;
	if(_x == x)
	{
		// Horizontal move
		int dx = (_y < y) ? 1 : -1;
		for (i = _y + dx; i != y; i += dx)
		{
			if (cBboard.getCell(_x, i) != nullptr)
			{
				return false;
			}
		}
	}
	else if (_y == y)
	{
		// Vertical move
		int dy = (_x < x) ? 1 : -1;
		for (i = _x + dy; i != x; i += dy)
		{
			if (cBboard.getCell(i, _y) != nullptr)
			{
				return false;
			}
		}
	}
	else
	{
		// Not a valid rook move
		return false;
	}
	return true;
}

