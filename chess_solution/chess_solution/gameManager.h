#pragma once
#include "bishop.h"
#include "king.h"
#include "knight.h"
#include "pawn.h"
#include "queen.h"
#include "rook.h"
#include "board.h"
#include <string>
#include "Pipe.h"
#include <map>

// turn defines
#define WHITE 0
#define BLACK 1
// general board defines
#define BOARD_LEN 64
#define ROW_LEN 8
#define COL_LEN 8
#define STARTING_BOARD_STR "RNBKQBNRPPPPPPPP################################pppppppprnbkqbnr"
// piece type defines
#define BISHOP 0
#define KING 1
#define KNIGHT 2
#define PAWN 3
#define QUEEN 4
#define ROOK 5


class gameMenager
{
public:
	gameMenager();
	~gameMenager();
	void createPieceBoard(board& cBboard);
	void getPoints(std::string movmentStr, int* currX, int* currY, int* newX, int* newY);
};