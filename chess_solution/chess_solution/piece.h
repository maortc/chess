#pragma once
#include <string>
#define WHITE 0
#define BLACK 1

#include "board.h"
class board;
class piece
{
public:
	piece();
	piece(int x, int y, bool color, int type);
	~piece();
	// getters and setters
	int getX();
	void setX(int x);
	int getY();
	void setY(int y);
	bool getColor();
	void setColor(bool color);
	int getType();
	void setType(int type);
	// virtual functions
	virtual bool isMoveValid(int x, int y , board& cBboard) = 0;

protected:
	int _y;
	int _x;
	bool _color;
	int _type; // will be a enum that represents all pieces
};
