#include "gameManager.h"

gameMenager::gameMenager()
{
	
}

gameMenager::~gameMenager()
{
}
/*
the fuction create a piece board
intput: the board object
output: none
*/
void gameMenager::createPieceBoard(board& cBboard)
{
	int x = 0;
	int y = 0;
	int currLoction = 0;

	std::string tempStr = STARTING_BOARD_STR;

	for (y = 0; y < ROW_LEN; y++)
	{
		for (x = 0; x < COL_LEN; x++)
		{
			// by letter will enter the corect piece into cell
			switch (tempStr[currLoction])
			{
			case 'r':
				cBboard.setCell(x, y, new rook(x, y, WHITE, ROOK));
				break;
			case 'n':
				cBboard.setCell(x, y, new knight(x, y, WHITE, KNIGHT));
				break;
			case 'b':
				cBboard.setCell(x, y, new bishop(x, y, WHITE, BISHOP));
				break;
			case 'k':
				cBboard.setCell(x, y, new king(x, y, WHITE, KING));
				break;
			case 'q':
				cBboard.setCell(x, y, new queen(x, y, WHITE, QUEEN));
				break;
			case 'p':
				cBboard.setCell(x, y, new pawn(x, y, WHITE, PAWN));
				break;
			case '#':
				cBboard.setCell(x, y, nullptr);
				break;
			case 'R':
				cBboard.setCell(x, y, new rook(x, y, BLACK, ROOK));
				break;
			case 'N':
				cBboard.setCell(x, y, new knight(x, y, BLACK, KNIGHT));
				break;
			case 'B':
				cBboard.setCell(x, y, new bishop(x, y, BLACK, BISHOP));
				break;
			case 'K':
				cBboard.setCell(x, y, new king(x, y, BLACK, KING));
				break;
			case 'Q':
				cBboard.setCell(x, y, new queen(x, y, BLACK, QUEEN));
				break;
			case 'P':
				cBboard.setCell(x, y, new pawn(x, y, BLACK, PAWN));
				break;
			default:
				break;
			}
			currLoction++;
		}
	}
}
 /*
 the function get a string that represent a movment and save value as int
 input: the string and vars as pointers to save loctioans
 output: none
 */
void gameMenager::getPoints(std::string movmentStr, int* currX, int* currY, int* newX, int* newY)
{
	*currX = movmentStr[0] - 'a';
	*currY = abs(movmentStr[1] - '0' -8);
	*newX =  movmentStr[2] - 'a';
	*newY = abs(movmentStr[3] - '0' - 8);
}




