﻿#include "Pipe.h"
#include <iostream>
#include <thread>
#include "bishop.h"
#include "board.h"
#include "king.h"
#include "knight.h"
#include "pawn.h"
#include "piece.h"
#include "queen.h"
#include "rook.h"
#include "gameManager.h"

int main()
{
	board mBoard;
	gameMenager mGameMaenger;
	std::string msgFromGraphics = "";
	mGameMaenger.createPieceBoard(mBoard);
	srand(time_t(NULL));
	Pipe p;
	bool isConnect = p.connect();
	std::string ans;
	int currX = 0;
	int currY = 0;
	int newX = 0;
	int newY = 0;
	int code = 0;

	while (!isConnect)
	{
		std::cout << "cant connect to graphics" << std::endl;
		std::cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << std::endl;
		std::cin >> ans;

		if (ans == "0")
		{
			std::cout << "trying connect again.." << std::endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else
		{
			p.close();
			return 0;
		}
	}
	char msgToGraphics[1024];
	strcpy_s(msgToGraphics, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR1");
	p.sendMessageToGraphics(msgToGraphics);
	msgFromGraphics = p.getMessageFromGraphics(); // getting movement to check
	mGameMaenger.getPoints(msgFromGraphics, &currX, &currY, &newX, &newY); // getting move ints from string
	code = mBoard.getCode(currX, currY, newX, newY);
	while (msgFromGraphics != "quit")
	{
		strcpy_s(msgToGraphics, "YOUR CODE");
		int r = rand() % 10;
		msgToGraphics[0] = (char)(code + '0');
		msgToGraphics[1] = 0;
		p.sendMessageToGraphics(msgToGraphics);
		msgFromGraphics = p.getMessageFromGraphics(); // getting movement to check
		mGameMaenger.getPoints(msgFromGraphics, &currX, &currY, &newX, &newY); // getting move ints from string
		code = mBoard.getCode(currX, currY, newX, newY); // getting code
	}
	p.close();
}