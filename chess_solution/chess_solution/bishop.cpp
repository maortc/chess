#include "bishop.h"
bishop::bishop()
{
}
bishop::bishop(int x, int y, bool color, int type) :
	piece(x, y, color, type)
{
}
bishop::~bishop()
{
}
/*
the function checks if attempted move is valid
input: x and y of attempted and board
output: is move valid
*/
bool bishop::isMoveValid(int x, int y, board& cBboard)
{
	int dx = 0;
	int dy = 0;
	int indexX = _x;
	int indexY = _y;

	// calculating which diagonal line was prased
	dx = (_x < x) ? 1 : -1;
	dy = (_y < y) ? 1 : -1;

	// loop checks if way to point is empty
	while (indexX != x && indexY != y) // while haven't reaced prased location 
	{
		indexX += dx;
		indexY += dy;
		if (indexX == x && indexY == y) // checks if reaced target cell
		{
			return true;
		}
		if (cBboard.getCell(indexX, indexY) != nullptr) // checks if come across a piece
		{
			return false;
		}
	}
	return false;
}
