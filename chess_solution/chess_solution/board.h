#pragma once
#include "piece.h"

// turn defines
#define WHITE 0
#define BLACK 1
// general board defines
#define BOARD_LEN 64
#define ROW_LEN 8
#define COL_LEN 8
#define STARTING_BOARD_STR "RNBKQBNRPPPPPPPP################################pppppppprnbkqbnr"
// piece type defines
#define BISHOP 0
#define KING 1
#define KNIGHT 2
#define PAWN 3
#define QUEEN 4
#define ROOK 5
// defines for get code
#define	VALID_MOVE 0
#define	VALID_CHECK 1
#define	EMPTY 2
#define	SAME_COLOR 3
#define	INVALID_CHECK 4
#define	INVALID_INDEX 5
#define	INVALID_MOVE 6
#define	SAME_INDEX 7


class piece;

class board
{
public:
	board();
	~board();
	bool isCheck(bool kingColor);
	int getCode(int currX, int currY, int newX, int newY);
	bool isPointInRange(int x, int y); // check if in board boundaries
	bool isPointSameLoction(int currX, int currY, int newX, int newY);
	piece* getCell(int x, int y);
	void setCell(int x, int y, piece* newPiece);
	void setTurn(bool newTurn);
	bool getTurn();

private:
	bool _turn;
	piece* _pBoard[8][8];
};	