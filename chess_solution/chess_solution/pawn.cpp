#include "pawn.h"

pawn::pawn()
{
	isFirstTurn = true;
}
pawn::pawn(int x, int y, bool color, int type) :
	piece(x, y, color, type)
{
	isFirstTurn = true;
}
pawn::~pawn()
{
}

/*
the function checks if attempted move is valid
input: x and y of attempted and board
output: is move valid
*/
bool pawn::isMoveValid(int x, int y, board& cBboard)
{
	// checks movment dirction
	if (this->getColor() == WHITE)
	{
		if (_y < y)
		{
			return false; // dirction is invalid
		}
	}
	else // black
	{
		if (_y > y)
		{
			return false; // dirction is invalid
		}
	}
	// if we reaced here dirction is valid
	// now we check if the movmet is a valid eating 
	if (abs(_y - y) == 1 && abs(_x - x) == 1 && cBboard.getCell(x, y) != nullptr && cBboard.getCell(x, y)->getColor() != _color)
	{
		return true;
	}
	// now we check a Straight movment
	if (x == _x)
	{
		if (this->isFirstTurn) // if it the first movment
		{
			if (abs(_y - y) == FIRST_MOVE_SIZE) // if movment is the size of 2 (allowed only in first movment)
			{
				if (cBboard.getCell(x, y - 1) == nullptr && cBboard.getCell(x, y) == nullptr)
				{
					this->isFirstTurn = false; // update that first move happpened
					return true;
				}
				return false;
			}
		}
		if (abs(_y - y) == RESET_MOVE_SIZE && cBboard.getCell(x, y) == nullptr)
		{
			this->isFirstTurn = false;
			return true;
		}
	}
	return false;
}
