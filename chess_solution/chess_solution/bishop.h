#pragma once
#include "piece.h"

class bishop : public piece
{
public:
	bishop();
	bishop(int x, int y, bool color, int type);
	~bishop();
	virtual bool isMoveValid(int x, int y, board& cBboard);
};

